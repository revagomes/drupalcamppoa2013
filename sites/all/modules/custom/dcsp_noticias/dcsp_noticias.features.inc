<?php
/**
 * @file
 * dcsp_noticias.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dcsp_noticias_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function dcsp_noticias_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function dcsp_noticias_node_info() {
  $items = array(
    'noticia' => array(
      'name' => t('Notícia'),
      'base' => 'node_content',
      'description' => t('Use <em>notícia</em> para postar notícias sobre a DrupalCamp SP.'),
      'has_title' => '1',
      'title_label' => t('Título'),
      'help' => '',
    ),
  );
  return $items;
}
