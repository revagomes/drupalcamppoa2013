<?php
/**
 * @file
 * dcsp_noticias.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dcsp_noticias_user_default_permissions() {
  $permissions = array();

  // Exported permission: create noticia content.
  $permissions['create noticia content'] = array(
    'name' => 'create noticia content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any noticia content.
  $permissions['delete any noticia content'] = array(
    'name' => 'delete any noticia content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own noticia content.
  $permissions['delete own noticia content'] = array(
    'name' => 'delete own noticia content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any noticia content.
  $permissions['edit any noticia content'] = array(
    'name' => 'edit any noticia content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own noticia content.
  $permissions['edit own noticia content'] = array(
    'name' => 'edit own noticia content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
