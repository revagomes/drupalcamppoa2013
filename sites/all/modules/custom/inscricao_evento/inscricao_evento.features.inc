<?php
/**
 * @file
 * inscricao_evento.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function inscricao_evento_commerce_product_default_types() {
  $items = array(
    'inscricao_em_evento' => array(
      'type' => 'inscricao_em_evento',
      'name' => 'Inscrição em evento',
      'description' => '',
      'help' => '',
      'revision' => '0',
    ),
    'product' => array(
      'type' => 'product',
      'name' => 'Product',
      'description' => 'A basic product type.',
      'help' => '',
      'revision' => '1',
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function inscricao_evento_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function inscricao_evento_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_default_registration_state().
 */
function inscricao_evento_default_registration_state() {
  $items = array();
  $items['canceled'] = entity_import('registration_state', '{
    "name" : "canceled",
    "label" : "Canceled",
    "description" : "Registration was cancelled",
    "default_state" : "0",
    "active" : "0",
    "show_on_form" : "0",
    "weight" : "1",
    "rdf_mapping" : []
  }');
  $items['complete'] = entity_import('registration_state', '{
    "name" : "complete",
    "label" : "Complete",
    "description" : "Registration has been completed.",
    "default_state" : "1",
    "active" : "1",
    "show_on_form" : "0",
    "weight" : "1",
    "rdf_mapping" : []
  }');
  $items['pending'] = entity_import('registration_state', '{
    "name" : "pending",
    "label" : "Pending",
    "description" : "Registration is pending.",
    "default_state" : "0",
    "active" : "0",
    "show_on_form" : "0",
    "weight" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_registration_type().
 */
function inscricao_evento_default_registration_type() {
  $items = array();
  $items['registration'] = entity_import('registration_type', '{
    "name" : "registration",
    "label" : "Registration",
    "locked" : "0",
    "weight" : "0",
    "data" : null,
    "rdf_mapping" : []
  }');
  return $items;
}
