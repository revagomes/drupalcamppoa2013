<?php
/**
 * @file
 * inscricao_evento.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function inscricao_evento_default_rules_configuration() {
  $items = array();
  $items['rules_enviar_email_order_status_autorizado'] = entity_import('rules_config', '{ "rules_enviar_email_order_status_autorizado" : {
      "LABEL" : "Enviar e-mail de comprova\\u00e7\\u00e3o quando o Status da Order for \\u0022Autorizado\\u0022",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "DrupalCampSP" ],
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_order_update" ],
      "IF" : [
        { "data_is" : { "data" : [ "commerce-order:status" ], "value" : "payment_authorized" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "commerce-order:mail" ],
            "subject" : "[DrupalCamp SP - 2013] Inscri\\u00e7\\u00e3o na DrupalCamp SP - 2013 confirmada",
            "message" : "Parab\\u00e9ns, sua inscri\\u00e7\\u00e3o na DrupalCamp SP - 2013 j\\u00e1 est\\u00e1 confirmada! \\r\\n\\r\\nContamos com sua presen\\u00e7a.",
            "from" : [ "site:mail" ],
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
