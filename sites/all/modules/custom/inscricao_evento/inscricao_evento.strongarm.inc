<?php
/**
 * @file
 * inscricao_evento.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function inscricao_evento_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'moip_debug';
  $strongarm->value = 0;
  $export['moip_debug'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'moip_detail_messages';
  $strongarm->value = 0;
  $export['moip_detail_messages'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'moip_order_reason_token';
  $strongarm->value = '[inscricao_evento:order-reason]';
  $export['moip_order_reason_token'] = $strongarm;

  return $export;
}
