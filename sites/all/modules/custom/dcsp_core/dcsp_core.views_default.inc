<?php
/**
 * @file
 * dcsp_core.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function dcsp_core_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'participantes';
  $view->description = 'Listagem de participantes do evento.';
  $view->tag = 'default';
  $view->base_table = 'users';
  $view->human_name = 'Participantes';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Quem Vai';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'mais';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Restaurar';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Ordenar por';
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'Crescente';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'responsive_grid';
  $handler->display->display_options['style_options']['column_classes'] = 'span2';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Campo: Usuário: Foto */
  $handler->display->display_options['fields']['picture']['id'] = 'picture';
  $handler->display->display_options['fields']['picture']['table'] = 'users';
  $handler->display->display_options['fields']['picture']['field'] = 'picture';
  $handler->display->display_options['fields']['picture']['label'] = '';
  $handler->display->display_options['fields']['picture']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['picture']['link_photo_to_profile'] = FALSE;
  /* Campo: Usuário: Nome */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['exclude'] = TRUE;
  /* Campo: Usuário: Nome completo */
  $handler->display->display_options['fields']['field_user_nome']['id'] = 'field_user_nome';
  $handler->display->display_options['fields']['field_user_nome']['table'] = 'field_data_field_user_nome';
  $handler->display->display_options['fields']['field_user_nome']['field'] = 'field_user_nome';
  $handler->display->display_options['fields']['field_user_nome']['label'] = '';
  $handler->display->display_options['fields']['field_user_nome']['alter']['path'] = 'user/[name]';
  $handler->display->display_options['fields']['field_user_nome']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_user_nome']['element_class'] = 'accordion-head';
  $handler->display->display_options['fields']['field_user_nome']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_user_nome']['element_default_classes'] = FALSE;
  /* Critério de ordenação: Global: Aleatório */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Critério de filtragem: Usuário: Ativo */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Critério de filtragem: Usuário: O ID do usuário */
  $handler->display->display_options['filters']['uid_raw']['id'] = 'uid_raw';
  $handler->display->display_options['filters']['uid_raw']['table'] = 'users';
  $handler->display->display_options['filters']['uid_raw']['field'] = 'uid_raw';
  $handler->display->display_options['filters']['uid_raw']['operator'] = '>';
  $handler->display->display_options['filters']['uid_raw']['value']['value'] = '1';
  /* Critério de filtragem: Usuário: Nome completo (field_user_nome) */
  $handler->display->display_options['filters']['field_user_nome_value']['id'] = 'field_user_nome_value';
  $handler->display->display_options['filters']['field_user_nome_value']['table'] = 'field_data_field_user_nome';
  $handler->display->display_options['filters']['field_user_nome_value']['field'] = 'field_user_nome_value';
  $handler->display->display_options['filters']['field_user_nome_value']['operator'] = 'word';
  $handler->display->display_options['filters']['field_user_nome_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_user_nome_value']['expose']['operator_id'] = 'field_user_nome_value_op';
  $handler->display->display_options['filters']['field_user_nome_value']['expose']['label'] = 'Nome';
  $handler->display->display_options['filters']['field_user_nome_value']['expose']['description'] = 'Buscar por nome';
  $handler->display->display_options['filters']['field_user_nome_value']['expose']['operator'] = 'field_user_nome_value_op';
  $handler->display->display_options['filters']['field_user_nome_value']['expose']['identifier'] = 'nome';
  $handler->display->display_options['filters']['field_user_nome_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'quem-vai';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Quem Vai';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $translatables['participantes'] = array(
    t('Master'),
    t('Quem Vai'),
    t('mais'),
    t('Aplicar'),
    t('Restaurar'),
    t('Ordenar por'),
    t('Crescente'),
    t('Desc'),
    t('Nome'),
    t('Buscar por nome'),
    t('Page'),
  );
  $export['participantes'] = $view;

  return $export;
}
