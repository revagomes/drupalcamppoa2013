<?php
/**
 * @file
 * dcsp_core.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function dcsp_core_user_default_roles() {
  $roles = array();

  // Exported role: organizador.
  $roles['organizador'] = array(
    'name' => 'organizador',
    'weight' => '3',
  );

  // Exported role: palestrante.
  $roles['palestrante'] = array(
    'name' => 'palestrante',
    'weight' => '4',
  );

  return $roles;
}
