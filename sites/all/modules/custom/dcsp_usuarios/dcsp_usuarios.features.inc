<?php
/**
 * @file
 * dcsp_usuarios.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dcsp_usuarios_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
