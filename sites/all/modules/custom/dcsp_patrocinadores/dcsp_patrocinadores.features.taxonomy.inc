<?php
/**
 * @file
 * dcsp_patrocinadores.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function dcsp_patrocinadores_taxonomy_default_vocabularies() {
  return array(
    'cotas_patrocinio' => array(
      'name' => 'Cotas de Patrocínio',
      'machine_name' => 'cotas_patrocinio',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
