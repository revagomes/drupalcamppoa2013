<?php
/**
 * @file
 * dcsp_patrocinadores.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dcsp_patrocinadores_user_default_permissions() {
  $permissions = array();

  // Exported permission: create patrocinador content.
  $permissions['create patrocinador content'] = array(
    'name' => 'create patrocinador content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any patrocinador content.
  $permissions['delete any patrocinador content'] = array(
    'name' => 'delete any patrocinador content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own patrocinador content.
  $permissions['delete own patrocinador content'] = array(
    'name' => 'delete own patrocinador content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete terms in 2.
  $permissions['delete terms in 2'] = array(
    'name' => 'delete terms in 2',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: edit any patrocinador content.
  $permissions['edit any patrocinador content'] = array(
    'name' => 'edit any patrocinador content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own patrocinador content.
  $permissions['edit own patrocinador content'] = array(
    'name' => 'edit own patrocinador content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit terms in 2.
  $permissions['edit terms in 2'] = array(
    'name' => 'edit terms in 2',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  return $permissions;
}
