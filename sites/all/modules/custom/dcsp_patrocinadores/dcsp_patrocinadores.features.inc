<?php
/**
 * @file
 * dcsp_patrocinadores.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dcsp_patrocinadores_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function dcsp_patrocinadores_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function dcsp_patrocinadores_node_info() {
  $items = array(
    'patrocinador' => array(
      'name' => t('Patrocinador'),
      'base' => 'node_content',
      'description' => t('Patrocinadores do Evento'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
