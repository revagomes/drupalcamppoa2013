<?php
/**
 * @file
 * dcsp_palestras.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function dcsp_palestras_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'palestras';
  $view->description = 'Listagem de palestras.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Palestras';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Palestras';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'mais';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Restaurar';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Ordenar por';
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'Crescente';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Comportamento quando sem resultados: Global: Área de texto */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'Ainda não há nenhuma palestra pendente.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Campo: Conteúdo: Título */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Critério de ordenação: Global: Aleatório */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Critério de filtragem: Conteúdo: Publicado */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Critério de filtragem: Conteúdo: Tipo */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'palestra' => 'palestra',
  );
  /* Critério de filtragem: Conteúdo: Status (field_palestra_status) */
  $handler->display->display_options['filters']['field_palestra_status_tid']['id'] = 'field_palestra_status_tid';
  $handler->display->display_options['filters']['field_palestra_status_tid']['table'] = 'field_data_field_palestra_status';
  $handler->display->display_options['filters']['field_palestra_status_tid']['field'] = 'field_palestra_status_tid';
  $handler->display->display_options['filters']['field_palestra_status_tid']['value'] = array(
    8 => '8',
    10 => '10',
  );
  $handler->display->display_options['filters']['field_palestra_status_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_palestra_status_tid']['vocabulary'] = 'status';

  /* Display: Aprovadas */
  $handler = $view->new_display('block', 'Aprovadas', 'block');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* Comportamento quando sem resultados: Global: Área de texto */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'Ainda não há nenhuma palestra aprovada.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Critério de filtragem: Conteúdo: Publicado */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Critério de filtragem: Conteúdo: Tipo */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'palestra' => 'palestra',
  );
  /* Critério de filtragem: Conteúdo: Status (field_palestra_status) */
  $handler->display->display_options['filters']['field_palestra_status_tid']['id'] = 'field_palestra_status_tid';
  $handler->display->display_options['filters']['field_palestra_status_tid']['table'] = 'field_data_field_palestra_status';
  $handler->display->display_options['filters']['field_palestra_status_tid']['field'] = 'field_palestra_status_tid';
  $handler->display->display_options['filters']['field_palestra_status_tid']['value'] = array(
    20 => '20',
  );
  $handler->display->display_options['filters']['field_palestra_status_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_palestra_status_tid']['vocabulary'] = 'status';
  /* Critério de filtragem: Conteúdo: Trilha (field_palestra_trilha) */
  $handler->display->display_options['filters']['field_palestra_trilha_tid']['id'] = 'field_palestra_trilha_tid';
  $handler->display->display_options['filters']['field_palestra_trilha_tid']['table'] = 'field_data_field_palestra_trilha';
  $handler->display->display_options['filters']['field_palestra_trilha_tid']['field'] = 'field_palestra_trilha_tid';
  $handler->display->display_options['filters']['field_palestra_trilha_tid']['value'] = '';
  $handler->display->display_options['filters']['field_palestra_trilha_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_palestra_trilha_tid']['expose']['operator_id'] = 'field_palestra_trilha_tid_op';
  $handler->display->display_options['filters']['field_palestra_trilha_tid']['expose']['label'] = 'Trilha';
  $handler->display->display_options['filters']['field_palestra_trilha_tid']['expose']['operator'] = 'field_palestra_trilha_tid_op';
  $handler->display->display_options['filters']['field_palestra_trilha_tid']['expose']['identifier'] = 'field_palestra_trilha_tid';
  $handler->display->display_options['filters']['field_palestra_trilha_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['field_palestra_trilha_tid']['vocabulary'] = 'trilhas';

  /* Display: Pendentes */
  $handler = $view->new_display('block', 'Pendentes', 'block_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Critério de filtragem: Conteúdo: Publicado */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Critério de filtragem: Conteúdo: Tipo */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'palestra' => 'palestra',
  );
  /* Critério de filtragem: Conteúdo: Status (field_palestra_status) */
  $handler->display->display_options['filters']['field_palestra_status_tid']['id'] = 'field_palestra_status_tid';
  $handler->display->display_options['filters']['field_palestra_status_tid']['table'] = 'field_data_field_palestra_status';
  $handler->display->display_options['filters']['field_palestra_status_tid']['field'] = 'field_palestra_status_tid';
  $handler->display->display_options['filters']['field_palestra_status_tid']['value'] = array(
    19 => '19',
  );
  $handler->display->display_options['filters']['field_palestra_status_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_palestra_status_tid']['vocabulary'] = 'status';
  $translatables['palestras'] = array(
    t('Master'),
    t('Palestras'),
    t('mais'),
    t('Aplicar'),
    t('Restaurar'),
    t('Ordenar por'),
    t('Crescente'),
    t('Desc'),
    t('Ainda não há nenhuma palestra pendente.'),
    t('Aprovadas'),
    t('Ainda não há nenhuma palestra aprovada.'),
    t('Trilha'),
    t('Pendentes'),
  );
  $export['palestras'] = $view;

  return $export;
}
