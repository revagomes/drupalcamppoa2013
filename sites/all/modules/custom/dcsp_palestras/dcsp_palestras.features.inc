<?php
/**
 * @file
 * dcsp_palestras.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dcsp_palestras_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function dcsp_palestras_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function dcsp_palestras_node_info() {
  $items = array(
    'palestra' => array(
      'name' => t('Palestra'),
      'base' => 'node_content',
      'description' => t('Use <em>palestra</em> para cadastrar palestras da DrupalCamp SP.'),
      'has_title' => '1',
      'title_label' => t('Título da Palestra'),
      'help' => '',
    ),
  );
  return $items;
}
