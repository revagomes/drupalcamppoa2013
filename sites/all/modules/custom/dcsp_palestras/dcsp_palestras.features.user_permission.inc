<?php
/**
 * @file
 * dcsp_palestras.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dcsp_palestras_user_default_permissions() {
  $permissions = array();

  // Exported permission: create field_palestra_hora.
  $permissions['create field_palestra_hora'] = array(
    'name' => 'create field_palestra_hora',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_palestra_status.
  $permissions['create field_palestra_status'] = array(
    'name' => 'create field_palestra_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create palestra content.
  $permissions['create palestra content'] = array(
    'name' => 'create palestra content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any palestra content.
  $permissions['delete any palestra content'] = array(
    'name' => 'delete any palestra content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own palestra content.
  $permissions['delete own palestra content'] = array(
    'name' => 'delete own palestra content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any palestra content.
  $permissions['edit any palestra content'] = array(
    'name' => 'edit any palestra content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit field_palestra_hora.
  $permissions['edit field_palestra_hora'] = array(
    'name' => 'edit field_palestra_hora',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_palestra_status.
  $permissions['edit field_palestra_status'] = array(
    'name' => 'edit field_palestra_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_palestra_hora.
  $permissions['edit own field_palestra_hora'] = array(
    'name' => 'edit own field_palestra_hora',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_palestra_status.
  $permissions['edit own field_palestra_status'] = array(
    'name' => 'edit own field_palestra_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own palestra content.
  $permissions['edit own palestra content'] = array(
    'name' => 'edit own palestra content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: view field_palestra_hora.
  $permissions['view field_palestra_hora'] = array(
    'name' => 'view field_palestra_hora',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_palestra_status.
  $permissions['view field_palestra_status'] = array(
    'name' => 'view field_palestra_status',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_palestra_hora.
  $permissions['view own field_palestra_hora'] = array(
    'name' => 'view own field_palestra_hora',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_palestra_status.
  $permissions['view own field_palestra_status'] = array(
    'name' => 'view own field_palestra_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
