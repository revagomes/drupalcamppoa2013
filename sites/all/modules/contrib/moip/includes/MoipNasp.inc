<?php

/**
 * @file Class encapsulating the business logic related to the processing the 
 * notifications data that MoIP sends back to the Drupal.
 */
class MoipNasp {

  private $moipData;
  private $order_id;
  private $payment_method = 'moip_api';

  /**
   * base constructor of class, it gets as parameter an $_POST from MOIP
   */
  public function __construct($moipData) {

    if (!empty($moipData)) {
      $this->moipData = $moipData;
      $id_transacao = explode('_', $this->moipData['id_transacao']);
      $this->order_id = $id_transacao[0];
    }
    else {
      throw new Exception('Moip data is null');
    }
  }

  public function saveTransaction() {

    // If this is a prior authorization capture NASP for which we've already
    // created a transaction...
    $priorData = $this->naspLoad($this->moipData['cod_moip']);

    if ($priorData) {
      // Load the prior NASP's transaction and update that with the capture values.
      $transaction = commerce_payment_transaction_load($priorData['transaction_id']);
    }
    else {
      // Create a new payment transaction for the order.
      $transaction = commerce_payment_transaction_new($this->payment_method, $this->order_id);
      $transaction->instance_id = $this->payment_method;
    }

    $transaction->remote_id = $this->moipData['cod_moip'];
    $transaction->amount = $this->moipData['valor'];
    // MoIP supports only Brazilian Reais.
    $transaction->currency_code = 'BRL';
    $transaction->payload[REQUEST_TIME] = $this->moipData;

    // Set the transaction's statuses based on the NASP's status_pagamento.
    $transaction->remote_status = $this->moipData['status_pagamento'];

    switch ($this->moipData['status_pagamento']) {
      case MOIP_STATUS_INITIALIZED:
      case MOIP_STATUS_PRINTED:
      case MOIP_STATUS_PENDING:
        $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
        break;
      case MOIP_STATUS_AUTHORIZED:
      case MOIP_STATUS_COMPLETED:
        $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
        break;
      case MOIP_STATUS_CANCELED:
      case MOIP_STATUS_REVERSED:
      case MOIP_STATUS_REFUNDED:
        $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
        break;
    }

    $transaction->message = $this->getStatusDescription($this->moipData['status_pagamento']);
    $transaction->message_variables = array();

    // Save the transaction information.
    commerce_payment_transaction_save($transaction);
    $this->moipData['transaction_id'] = $transaction->transaction_id;

    watchdog('moip', t('NASP was processed for Order @order_number, with ID @cod_moip, changing it status to @status'), array(
      '@cod_moip' => $this->moipData['cod_moip']
      , '@order_number' => $this->order_id
      , '@status' => $this->getCorrespondingStatus($this->moipData['status_pagamento'])
      )
      , WATCHDOG_INFO);
  }

  /**
   * Update drupal order status, according to data from Nasp, doing the 
   * correlation with MoIP and Drupal Commerce statuses.
   */
  public function setOrderStatus() {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $this->order_id);
    $order_wrapper->status = $this->getCorrespondingStatus($this->moipData['status_pagamento']);
    $order_wrapper->save();
  }

  /**
   * Persist data from Nasp to database
   * @return boolean
   */
  public function saveToDatabase() {

    $this->moipData = $this->moipData;
    $priorData = $this->naspLoad($this->moipData['cod_moip'], 'cod_moip');

    if (!empty($this->moipData['cod_moip']) && $priorData) {
      if ($priorData['id_transacao'] != $this->moipData['id_transacao']) {
        watchdog('moip', "NASP Transaction id's doesn't match");
        //throw new Exception("Error NASP Transaction id's doesn't match");
      }
      else {
        $this->moipData['changed'] = REQUEST_TIME;
        return drupal_write_record('moip_nasp', $this->moipData, 'id_transacao');
      }
    }
    else {

      $this->moipData['created'] = REQUEST_TIME;
      $this->moipData['changed'] = REQUEST_TIME;

      return drupal_write_record('moip_nasp', $this->moipData);
    }
  }

  /**
   * Return a list of payment statuses, or an specific payment status message.
   *
   * @param $status
   *   The status identification in which to return the message.
   */
  public function getStatusDescription($status = NULL) {
    $statuses = array(
      MOIP_STATUS_INITIALIZED => t('The payment is not done yet, or user has abandoned the payment process.'),
      MOIP_STATUS_PRINTED => t('The payment receipt "Boleto" was printed. Awaiting offline payment.'),
      MOIP_STATUS_PENDING => t('The customer paid with a credit card and the payment is awaiting manual review from MoIP team.'),
      MOIP_STATUS_AUTHORIZED => t('The payment was authorized but not completed yet, due to the normal flow of chosen payment method.'),
      MOIP_STATUS_COMPLETED => t('The payment was completed and the money was credited in the recipient account.'),
      MOIP_STATUS_CANCELED => t('The payment was canceled by the payer, payment institution, MoIP or recipient account.'),
      MOIP_STATUS_REVERSED => t('The payment was reversed by the payer.'),
      MOIP_STATUS_REFUNDED => t('The payment was refunded to the payer.'),
    );
    return empty($status) ? $statuses : $statuses[$status];
  }

  /**
   * Returns the corresponding status from Drupal Commerce, for the id used in
   * MoIP
   *
   * @param int $remote_status_id
   *   The status from MoIP
   * @return string
   */
  public function getCorrespondingStatus($remote_status_id) {
    $statuses = array(
      MOIP_STATUS_INITIALIZED => 'pending',
      MOIP_STATUS_PRINTED => 'pending',
      MOIP_STATUS_PENDING => 'processing',
      MOIP_STATUS_AUTHORIZED => 'payment_authorized',
      MOIP_STATUS_COMPLETED => 'completed',
      MOIP_STATUS_CANCELED => 'canceled',
      MOIP_STATUS_REVERSED => 'canceled',
      MOIP_STATUS_REFUNDED => 'canceled'
    );
    if (!in_array($remote_status_id, array_keys($statuses))) {
      watchdog('moip', t('The payment status sent by MoIP was not recognized by 
        MoIP module. Please file an issue at http://drupal.org/project/moip.'));
      throw new Exception;
    }
    return $statuses[$remote_status_id];
  }

  /**
   * Load a Nasp from commerce_moip_nasp table 
   * based on id and column   
   */
  public function naspLoad($id, $type = 'cod_moip') {
    $query = db_select('moip_nasp', 'cmn')
      ->fields('cmn')
      ->condition($type, $id)
      ->execute()
      ->fetchAssoc();
    return empty($query) ? FALSE : $query;
  }

}