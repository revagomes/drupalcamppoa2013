<?php

/**
 * Process the $POST data that MoIP sends in the NASP integration
 * See more about hte integration at https://www.moip.com.br/pdf/Notificar.pdf
 */
function moip_nasp_process_page() {
  try {

    if (variable_get('moip_debug', FALSE)) {
      watchdog('moipdbg_nasp', '<pre>' . print_r($_POST, TRUE) . '</pre>');
    }

    // Checks what is being passed to the url, preventing hijacks
    if (empty($_POST['id_transacao']) || empty($_POST['cod_moip'])) {
      watchdog('moip', t('An attempt to access NASP notification url was made, with wrong parameters. See below:') . ' <pre>' . print_r($_REQUEST, TRUE) . '</pre>'
        , array()
        , WATCHDOG_ERROR);
      throw new Exception(t('Wrong parameters was sent to the MoIP integration notification page'));
    }

    $nasp = new MoipNasp($_POST);
    $nasp->saveTransaction();
    $nasp->setOrderStatus();
    $nasp->saveToDatabase();
  } catch (Exception $e) {
    print $e->getMessage();
    drupal_exit();
  }
}

